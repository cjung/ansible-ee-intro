# ansible-ee-intro

Repository with introduction and demo content for Ansible execution environments. Find more details about how to use `ansible-builder` on the [Documentation](https://ansible-builder.readthedocs.io/en/latest/) web site. More information on `ansible-navigator` can be found in the [GitHub Project](https://github.com/ansible/ansible-navigator).

This material was created for the talk [Meetup 08.2021 - Was gibt es Neues im Ansible Universum?](https://www.meetup.com/de-DE/Ansible-Meetup-Dresden/events/279708247). The slides from this talk have been [archived in this repository](./whats-new-in-the-ansible-universe.pdf) as well.

## ansible-builder

You can install `ansible-builder` by using `pip` (it's recommended, but not required, to use a dedicated virtual environment):

```bash
virtualenv ansible-builder
. ansible-builder/bin/activate
pip install ansible-builder
```

If you're a Red Hat customer and have access to an Red Hat Ansible Automation Platform subscription, you can enable the Red Hat Ansible Automation Platform repository and install `ansible-builder` by using `yum`.

### How to build it

Build the image:

```bash
tag=0.1.0
# -v 3 is optional but gives helpful debug output
ansible-builder build -f ee-ansible-demo.yml -t ee-ansible-demo:$tag -v 3
```

If you use Red Hat Enterprise Linux with an active Subscription, you might have to lo log into the registry first:

```bash
podman login registry.redhat.io
# enter your RHN credentials
```

### Push the image

Typically you want to push your execution environment to a registry, or private automation hub. The example below uses the builtin registry of GitLab.

```bash
tag=0.1.0
# push image to registry
podman login registry.gitlab.com
podman push ee-ansible-demo:$tag registry.gitlab.com/cjung/ansible-ee-intro/ee-ansible-demo:$tag
podman push ee-ansible-demo:$tag registry.gitlab.com/cjung/ansible-ee-intro/ee-ansible-demo:latest
```

## ansible-navigator

The traditional `ansible-playbook` command does not leverage execution environments. Use `ansible-navigator` to run Ansible Playbooks inside execution environments.

You can install `ansible-navigator` by using `pip` (it's recommended, but not required, to use a dedicated virtual environment):

```bash
virtualenv ansible-navigator
. ansible-navigator/bin/activate
pip install ansible-navigator
```

If you're a Red Hat customer and have access to an Red Hat Ansible Automation Platform subscription, you can enable the Red Hat Ansible Automation Platform repository and install `ansible-navigator` by using `yum`.

Although `ansible-navigator` has a comprehensive command line, it might be easier to store some defaults in the `ansible-navigator.yml` configuration file. Use the following example as a reference:

```yaml
---
ansible-navigator:
  ansible:
    inventories:
    # instead of specifying -i on the CLI, you can point to your inventory for additional convenience
      - /path/to/your/ansible/inventory.yml
  logging:
    # debug level can be very helpful to better understand how ansible-navigator works
    level: debug
    append: true
    # by default the log file is in your working directory which might not be ideal
    file: /tmp/navigator/ansible-navigator.log
  playbook-artifact:
    # ansible-navigator can collect artifacts to analyze previous ansible-playbook outputs
    enable: false
  execution-environment:
    # only pull execution environment if not already available, set this to always to always check for the latest EE before running the Playbook
    pull-policy: missing
    image: registry.gitlab.com/cjung/ansible-ee-intro/ee-ansible-demo
    environment-variables:
      # you can pass certain environment variables onto the EE, this can be very helpful e.g. to forward cloud provider credentials
      pass:
        - AWS_ACCESS_KEY_ID
        - AWS_SECRET_ACCESS_KEY
        - CONTROLLER_HOST
        - CONTROLLER_USERNAME
        - CONTROLLER_PASSWORD
        - CONTROLLER_VERIFY_SSL
```
